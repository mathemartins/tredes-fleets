from django.contrib import admin


# Register your models here.
from trips.models import Trips


class TripsAdmin(admin.ModelAdmin):
    list_display = ('from_locale', 'to_locale', 'bus', 'price', 'ticket_key', 'active', 'trip_time', 'timestamp')
    list_display_links = ('from_locale', 'to_locale')
    list_editable = ('price', 'active', 'trip_time')
    list_filter = ('from_locale', 'to_locale', 'trip_time', 'price')
    search_fields = ('from_locale', 'to_locale', 'price')

    prepopulated_fields = {'slug':('from_locale', 'to_locale', 'bus', 'price',)}
    readonly_fields = ('ticket_key',)

    ordering = ('-timestamp',)


admin.site.register(Trips, TripsAdmin)
