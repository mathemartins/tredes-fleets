from django.urls import path

from trips.views import TripList, TripDetails

urlpatterns = [
    path('', TripList.as_view(), name='trip-list'),
    path('<slug:slug>/details/', TripDetails.as_view(), name='trip-detail'),
]