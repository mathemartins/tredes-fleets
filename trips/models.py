from django.contrib.auth import get_user_model
from django.db import models

# Create your models here.
from django.db.models import Q
from django.db.models.signals import post_save
from django.urls import reverse

from bus.models import Bus
from tredes.utils import unique_key_generator

User = get_user_model()


class TripsQuerySet(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)

    def search(self, query):
        lookups = (Q(from_locale__icontains=query) | Q(to_locale__icontains=query) | Q(bus__icontains=query))
        return self.filter(lookups).distinct()


class TripsManager(models.Manager):
    def get_queryset(self):
        return TripsQuerySet(self.model, using=self._db)

    def all(self):
        return self.get_queryset().active()

    def get_by_id(self):
        qs = self.get_queryset().filter(id=id)
        if qs.count == 1:
            return qs.first()
        return None

    def search(self, query):
        return self.get_queryset().active().search(query)


class Trips(models.Model):
    from_locale = models.CharField(max_length=300)
    to_locale = models.CharField(max_length=300)
    bus = models.ForeignKey(Bus, on_delete=models.CASCADE)
    price = models.PositiveIntegerField(default=5000)
    active = models.BooleanField(default=True)
    trip_time = models.DateTimeField(blank=True, null=True)
    ticket_key = models.CharField(max_length=100, blank=True, null=True)
    available_seats = models.IntegerField(blank=True, null=True)
    slug = models.SlugField(unique=True, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    objects = TripsManager()

    # reverse relationship
    # bus_set = Bus.trips_set.all().count()

    class Meta:
        db_table = "trips"
        verbose_name = "trips"
        verbose_name_plural = "trips"

    def __str__(self):
        return self.from_locale

    def get_absolute_url(self):
        return reverse("trips-url:trip-detail", kwargs={"slug": self.slug})


def ticket_key_post_save_receiver(sender, created, instance, *args, **kwargs):
    print(sender, instance, created)
    if created:
        if not instance.available_seats:
            instance.available_seats = instance.bus.seater
            instance.save()
        if not instance.ticket_key:
            instance.ticket_key = unique_key_generator(instance)
            instance.save()
    elif instance.ticket_key is None:
        instance.ticket_key = unique_key_generator(instance)
        instance.save()
    else:
        pass


post_save.connect(ticket_key_post_save_receiver, sender=Trips)