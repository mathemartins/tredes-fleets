import datetime

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse
from django.utils import timezone
from django.views.generic import ListView, DetailView

from bookings.models import Booking
from trips.models import Trips


class TripList(LoginRequiredMixin, ListView):
    queryset = Trips.objects.all()

    def get_context_data(self, *args, **kwargs):
        context = super(TripList, self).get_context_data(*args, **kwargs)
        context['currentDateTime'] = timezone.now()

        return context

    def get_queryset(self):
        return Trips.objects.filter(active=True)


class TripDetails(LoginRequiredMixin, DetailView):
    queryset = Trips.objects.all()

    def get_context_data(self, *args, **kwargs):
        context = super(TripDetails, self).get_context_data(*args, **kwargs)
        context['booked_seats'] = self.get_object().bus.seater - self.get_object().available_seats
        context['currentDateTime'] = timezone.now()
        return context

    def render_to_response(self, context, **response_kwargs):
        if context:
            try:
                if self.get_object().trip_time <= timezone.now() or self.get_object().available_seats == 0:
                    messages.warning(self.request, "Booking For This Trip Has Expired!")
                booking_obj = Booking.objects.get(user=self.request.user, key=self.get_object().ticket_key)
                if booking_obj and booking_obj.token:
                    messages.info(self.request, "You have booked this trip already!")
            except:
                pass
        return super(TripDetails, self).render_to_response(context, **response_kwargs)

    def get_object(self, *args, **kwargs):
        slug = self.kwargs.get('slug')
        try:
            instance = Trips.objects.get(slug=slug)
        except Trips.DoesNotExist:
            return redirect(reverse('404_'))
        except Trips.MultipleObjectsReturned:
            qs = Trips.objects.filter(slug=slug)
            instance = qs.first()
        except:
            return redirect(reverse('404_'))
        return instance
