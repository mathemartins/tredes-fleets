import os
import random

from cloudinary.models import CloudinaryField
from django.db import models

# Create your models here.
from django.db.models import Q
from django.urls import reverse


def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_image_path(instance, filename):
    new_filename = random.randint(1, 3910209312)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "profile/{new_filename}/{final_filename}".format(
        new_filename=new_filename,
        final_filename=final_filename
    )


STATUS_CHOICES = (('A', 'Active'), ('C', 'Cancelled'),)


class CompanyAliasQuerySet(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)

    def featured(self):
        return self.filter(featured=True)

    def search(self, query):
        lookups = (Q(name__icontains=query) | Q(description__icontains=query))
        return self.filter(lookups).distinct()


class CompanyAliasManager(models.Manager):
    def get_queryset(self):
        return CompanyAliasQuerySet(self.model, using=self._db)

    def all(self):
        return self.get_queryset().active()

    def featured(self):
        return self.get_queryset().featured()

    def get_by_id(self):
        qs = self.get_queryset().filter(id=id)
        if qs.count == 1:
            return qs.first()
        return None

    def search(self, query):
        return self.get_queryset().active().search(query)


class CompanyAlias(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, blank=True, null=True)
    active = models.BooleanField(default=True)
    featured = models.BooleanField(default=False)
    image = CloudinaryField(upload_image_path, null=True, blank=True)
    slug = models.SlugField(unique=True, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    objects = CompanyAliasManager()

    class Meta:
        db_table = "company-alias"
        verbose_name = "company-alias"
        verbose_name_plural = "company-aliases"

    def __str__(self):
        return self.name

    def image_tag(self):
        from django.utils.html import escape, mark_safe
        return mark_safe('<img src="%s" width="500" height="300" />' % self.image.url)

    image_tag.short_description = 'Company Logo Image'
    image_tag.allow_tags = True

    def get_absolute_url(self):
        return reverse("url-namespace:url-name", kwargs={"slug": self.slug})

    @property
    def title(self):
        return self.name


VEHICLE_STATUS = (('Active', 'Active'), ('Breakdown', 'Breakdown'),)
VEHICLE_TYPE = (('Regular', 'Regular'), ('Luxury', 'Luxury'),)


class BusQuerySet(models.query.QuerySet):
    def condition_ok(self):
        return self.filter(condition__iexact="Active")

    def condition_bad(self):
        return self.filter(condition__iexact="Breakdown")

    def vehicle_type_R(self):
        return self.filter(condition__iexact="Regular")

    def vehicle_type_L(self):
        return self.filter(condition__iexact="Luxury")


class BusManager(models.Manager):
    def get_queryset(self):
        return BusQuerySet(self.model, using=self._db)

    def all(self):
        return self.get_queryset()

    def regular(self):
        return self.get_queryset().condition_ok().vehicle_type_R()

    def luxury(self):
        return self.get_queryset().condition_ok().vehicle_type_L()

    def condition_ok(self):
        return self.get_queryset().condition_ok()

    def condition_bad(self):
        return self.get_queryset().condition_bad()


class Bus(models.Model):
    company_alias = models.ForeignKey(CompanyAlias, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    image = CloudinaryField(upload_image_path, null=True, blank=True)
    seater = models.IntegerField(default=50)
    condition = models.CharField(max_length=10, choices=VEHICLE_STATUS, blank=True, null=True)
    plate_number = models.CharField(max_length=100)
    vehicle_type = models.CharField(max_length=10, choices=VEHICLE_TYPE, blank=True, null=True)
    color = models.CharField(max_length=20)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    objects = BusManager()

    class Meta:
        db_table = "bus"
        verbose_name = "bus"
        verbose_name_plural = "buses"

    def __str__(self):
        return "{} from {}".format(self.name, self.company_alias)

    def image_tag(self):
        from django.utils.html import escape, mark_safe
        return mark_safe('<img src="%s" width="500" height="300" />' % self.image.url)

    image_tag.short_description = 'Bus Image'
    image_tag.allow_tags = True

