from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Bus, CompanyAlias


# Register your models here.

@admin.register(Bus)
class BusAdmin(admin.ModelAdmin):
    list_display = ('company_alias', 'name', 'seater', 'plate_number', 'vehicle_type', 'color')
    list_display_links = ('company_alias', 'name', 'plate_number',)
    list_filter = ('company_alias', 'seater', 'vehicle_type', 'color',)
    readonly_fields = ('image_tag',)
    search_fields = ('company_alias', 'name')
    ordering = ('-timestamp',)


@admin.register(CompanyAlias)
class CompanyAliasAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'status', 'updated', 'timestamp')
    list_display_links = ('name',)
    list_filter = ('status',)
    readonly_fields = ('image_tag',)
    search_fields = ('description', 'name')
    ordering = ('-timestamp',)

    prepopulated_fields = {"slug": ("name",)}
