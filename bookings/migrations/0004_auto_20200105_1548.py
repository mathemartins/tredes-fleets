# Generated by Django 3.0.2 on 2020-01-05 23:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0003_booking_token'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='bus',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='from_location',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='to_destination',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='booking',
            name='key',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='booking',
            name='token',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
