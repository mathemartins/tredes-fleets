from django.contrib import admin

# Register your models here.

from bookings.models import Booking


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    list_display = ('user', 'key', 'seats', 'trip', 'token', 'bus', 'from_location', 'to_destination', 'timestamp', 'updated')
    list_display_links = ('user', 'key')
    list_filter = ('user', 'key', 'seats', 'trip')
    search_fields = ('key',)

    readonly_fields = ('user', 'key', 'seats', 'trip', 'token', 'bus', 'from_location', 'to_destination')