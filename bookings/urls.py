from django.urls import path

from bookings.views import SeatProcessor

urlpatterns = [
    path('data-processor/', SeatProcessor.as_view(), name='data-processor'),
]
