from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView
from django.views.generic.base import View

from bookings.models import Booking
from bus.models import Bus
from tredes.mixins import AjaxRequiredMixin
from trips.models import Trips

User = get_user_model()


class SeatProcessor(LoginRequiredMixin, AjaxRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        data = request.POST
        key = data['key']
        seats = data['seats']
        token = data['token']

        try:
            trip_obj = Trips.objects.get(ticket_key=key)
        except:
            trip_obj = Trips.objects.filter(ticket_key=key).first()

        # TODO: Add Booking Event To Calender

        # submit data to database
        exists = Booking.objects.filter(token=token, user=request.user.pk, key=key, seats=seats).exists()
        if exists:
            return JsonResponse({}, status=401)

        Booking.objects.create(
            user=request.user,
            trip=trip_obj,
            key=key,
            seats=seats,
            from_location=trip_obj.from_locale,
            to_destination=trip_obj.to_locale,
            bus=trip_obj.bus,
            token=token
        )
        # Update seat status
        bus_seater = trip_obj.bus.seater  # get the particular bus for that trip and the particular seat
        updated_ = bus_seater - int(seats)

        # get the trip and update the available seat
        trip_obj.available_seats = updated_
        trip_obj.save()

        data = {"success": True}
        return JsonResponse(data)
