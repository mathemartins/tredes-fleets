from django.contrib.auth import get_user_model
from django.db import models

from trips.models import Trips

User = get_user_model()


class Booking(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    trip = models.ForeignKey(Trips, on_delete=models.CASCADE, blank=True, null=True)
    key = models.CharField(max_length=200, blank=True, null=True)
    token = models.CharField(max_length=200, blank=True, null=True)
    seats = models.PositiveIntegerField()
    bus = models.CharField(max_length=200, blank=True, null=True)
    from_location = models.CharField(max_length=200, blank=True, null=True)
    to_destination = models.CharField(max_length=200, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "booking"
        verbose_name = "booking"
        verbose_name_plural = "bookings"
        ordering = ('-timestamp',)

    def __str__(self):
        return "Trip From {} to {} By {}".format(self.from_location, self.to_destination, self.bus)
