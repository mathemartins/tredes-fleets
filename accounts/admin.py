from django.contrib import admin
from .models import Profile


# Register your models here.

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'uuid', 'phone', 'slug', 'updated')
    list_display_links = ('user',)
    list_filter = ('user', 'uuid', 'phone')
    readonly_fields = ('slug', 'image_tag')
    search_fields = ('uuid', 'user')

    ordering = ('-timestamp',)
    fieldsets = (
        ('Basic Information', {'description': "Copy User UUID From Firebase Using Corresponding User Phone Number",
                               'fields': ('image_tag', 'uuid', 'phone', 'image', ('user',))}),
        ('Complete Full Information', {'classes': ('collapse',), 'fields': ('bio', ('gender', 'birthday'))}),)


admin.site.site_header = 'TREDES FLEETS'
