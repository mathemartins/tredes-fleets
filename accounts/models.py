import random
import os

from django.db import models
from django.db.models.signals import pre_save, post_save
from django.contrib.auth import get_user_model
from cloudinary.models import CloudinaryField
from django.dispatch import receiver
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField
from tredes.utils import unique_slug_generator

# Create your models here.

User = get_user_model()


def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_image_path(instance, filename):
    new_filename = random.randint(1, 3910209312)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "profile/{new_filename}/{final_filename}".format(
        new_filename=new_filename,
        final_filename=final_filename
    )


GENDER_CHOICES = (('M', 'Male'), ('F', 'Female'),)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    uuid = models.CharField(max_length=100, blank=True, null=True,
                            help_text="Copy This Data From Firebase UUID AccountKit")
    bio = models.TextField(blank=True, null=True)
    phone = PhoneNumberField()
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, blank=True, null=True)
    birthday = models.DateField(null=True, blank=True)
    image = CloudinaryField(upload_image_path, null=True, blank=True)
    slug = models.SlugField(unique=True, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "profile"
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"
        unique_together = ('phone', 'slug')

    def __str__(self):
        return str(self.user.username)

    def get_absolute_url(self):
        return reverse('account-url:profile', kwargs={'slug':self.slug})

    def age(self):
        import datetime
        return int((datetime.date.today() - self.birthday).days / 365.25)

    def image_tag(self):
        from django.utils.html import escape, mark_safe
        return mark_safe('<img src="%s" width="100" height="100" />' % self.image.url)

    image_tag.short_description = 'Profile Image'
    image_tag.allow_tags = True

    def get_name(self):
        if self.user.first_name and self.user.last_name:
            caps_initials = "{} {}".format(self.user.first_name, self.user.last_name)
            caps_initials = caps_initials.title()
            return caps_initials
        return self.user.username

    def get_firebase_id(self):
        if self.uuid:
            return self.uuid
        return "GET UUID FROM TREDES FLEETS MOBILE"

    @property
    def get_image(self):
        if self.image:
            return self.image.url
        return "https://img.icons8.com/officel/2x/user.png"


def post_save_profile_receiver(sender, created, instance, *args, **kwargs):
    if created:
        if not instance.slug:
            instance.slug = unique_slug_generator(instance)
            instance.save()


post_save.connect(post_save_profile_receiver, sender=Profile)
