from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic.base import View

from accounts.models import Profile


class DashboardView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        # Profile.objects.get_or_create(user=request.user, phone='+23411223344')
        context = {}
        return render(self.request, template_name='dashboard.html')